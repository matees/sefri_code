function [] = plotYCC(name)

%% 
% Esta función obtiene y dibuja las componentes YCC de la imagen que
% se pasa como párametro. Primero se pintan las componentes en
% escala de grises, y después siguiendo un modelo RGB
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

%Se obtienen las componentes YCbCr de la imagen
yCbCrComponents = rgb2ycbcr(rgbComponents);

%Se separa cada una de las componentes para ser mostradas en escala de grises
y = yCbCrComponents(:,:,1);
Cb = yCbCrComponents(:,:,2);
Cr = yCbCrComponents(:,:,3);
yComponent = cat(3,y,y,y);
CbComponent = cat(3,Cb,Cb,Cb);
CrComponent = cat(3,Cr,Cr,Cr);

%Se muestra por pontalla cada componente YCbCr en escala de grises
images = {rgbComponents,yComponent,CbComponent,CrComponent};
plotTemplate(images,'Componentes YCbCr en escala de grises');

%Se genera una matriz de ceros con la misma resolución de la imagen
res = size(rgbComponents);
if(isa(yCbCrComponents,'uint8'))
    a = (intmax(class(yCbCrComponents))/2)*uint8(ones(res(1:2)));
else
    a = (intmax(class(yCbCrComponents))/2)*uint16(ones(res(1:2)));
end

%Se separa individualmente cada una de las componentes para
%ser mostradas con un modelo RGB
Cbrgb = cat(3,a,Cb,a);
Cbrgb = ycbcr2rgb(Cbrgb);
Crrgb = cat(3,a,a,Cr);
Crrgb = ycbcr2rgb(Crrgb);

%Se muestra por pontalla cada componente YCbCr en RGB
images = {rgbComponents,yComponent,Cbrgb,Crrgb};
plotTemplate(images,'Componentes YCbCr en color');

