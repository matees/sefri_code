function [] = andyEffect(name)

%%
% Esta funcion genera una composicion a imitación de las obras de Andy Warhol
% a partir de la imagen que se le pasa como parametro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

image = imread(name);

imgEnhanced1 = imadjust(image,[0.00 0.00 0.00; 1.00 0.38 0.40],[1.00 0.00 0.70; 0.20 1.00 0.40], [4.90 4.00 1.70]);
imgEnhanced2 = imadjust(image,[0.13 0.00 0.30; 0.75 1.00 1.00],[0.00 1.00 0.50; 1.00 0.00 0.27], [5.90 0.80 4.10]);
imgEnhanced3 = imadjust(image,[0.20 0.00 0.09; 0.83 1.00 0.52],[0.00 0.00 1.00; 1.00 1.00 0.00], [1.10 2.70 1.00]);
imgEnhanced4 = imadjust(image,[0.20 0.00 0.00; 0.70 1.00 1.00],[1.00 0.90 0.00; 0.00 0.90 1.00], [1.30 1.00 1.00]);

r = fliplr(imgEnhanced2(:,:,1));
g = fliplr(imgEnhanced2(:,:,2));
b = fliplr(imgEnhanced2(:,:,3));
imgEnhanced2 = cat(3,r,g,b);

r = fliplr(imgEnhanced4(:,:,1));
g = fliplr(imgEnhanced4(:,:,2));
b = fliplr(imgEnhanced4(:,:,3));
imgEnhanced4 = cat(3,r,g,b);

images = {imgEnhanced1,imgEnhanced2,imgEnhanced3,imgEnhanced4};
plotTemplate(images, 'Efecto tipo Andy Warhol');