function [] = blackandwhite(name)

%% 
% Esta función pasa la imagen a blanco y negro. Además, invierte blanco
% y negro entre sí en la imagen resultante. Muestra el resultado de
% ambas operaciones comparadas con la imagen original.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

%Se obtienen las componentes YCbCr de la imagen
yCbCrComponents = rgb2ycbcr(rgbComponents);

% El factor para transformar las imagenes a blanco y negro depende del
% numero de bits utilizados para representar el color de la imagen
factor = intmax(class(rgbComponents));

%Se pasa la imagen a blanco y negro puro
y = yCbCrComponents(:,:,1);
y = round(y/factor)*factor; 
yComponent = cat(3,y,y,y);

%Se invierten el blanco y negro
%yInv = uint8(abs(double(y)-double(factor)));
%yInvComponent = cat(3,yInv,yInv,yInv);

%yComponent = imadjust(rgbComponents, [0 1], [1 0]);

%Se muestra por pontalla el resultado
images = {rgbComponents,yComponent};
%order = [1 3 4];
plotTemplate(images,'Convertir a blanco y negro');

