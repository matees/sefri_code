function [] = flipVertical(name)

%% 
% Esta función invierte siguiendo el eje de simetría vertical la imagen que
% se pasa como primer párametro
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgb = imread(name);
in = rgb;
res = size(rgb);

%Se invierte el orden de las columnas de las componentes RGB de la imagen
for ii = 1:3
    index = flip(1:res(2));
    rgb(:,:,ii) = rgb(:,index,ii);
end

%Se muestra por pontalla el efecto del cambio de simetria
images = {in,rgb};
plotTemplate(images,'Mirror Vertical');



