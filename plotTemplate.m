function [] = plotTemplate(images, title, order, extra)
    
%% 
% Esta función representa gráficamente las imágenes que se le pasan como
% primer parámetro en las posiciones especificadas por el segundo
% parámetro. Ademas permite mostrar informacion adicional relacionada con
% las imagenes que se representan (SSIM, PSNR, Perfil ICC, ...).
%
% Parametros de entrada:
%     image: cell array que contiene el conjunto de imagenes a mostrar en 
%            formato RGB 
%     order: vector que indica las posiciones dentro del subplot donde van 
%            a aparecer las imagenes
%     extra: cell array del mismo tamaño que image y que contiene los mensajes 
%            que se quieren mostrar asociados a cada imagen mostrada

%%

% Se determinan los puntos de referencia para cada una de las imagenes a partir
% del numero de imagenes que se tienen que mostrar y de su posicion en la
% pantalla.
nImages = size(images,2);
if(nargin<3)
    order = 1:nImages;
end
if(nargin<4)
    extra = [];
    absHeight = 0.49;
else
    absHeight = 0.44;
end

nRows = ceil(nImages/2);
gap = 1/nRows;
ini = 0.01;
if(nRows ==1)
    width = 0.49;
    height = 0.8;
else
    width = 0.49;
    height = absHeight;
end

res = size(images{1});
rate = res(1)/res(2);


% Las imagenes se colocan en una posicion adecuada dentro de la figura para 
% que puedan ser visualizadas de forma correcta, sin solopamientos y con el 
% mayor tamaño posible.
fullfig('Name', title);
for ii = 1:nImages
    if(mod(order(ii),2))
        left = 0.01;
    else
        left = 0.51;
    end
    bottom = ini + gap*(nRows-ceil(order(ii)/2));
    if(nRows==1)
        bottom = 0.1;
    end
    h(ii) = subplot(nRows,2,order(ii));
    imshow(images{ii});
    set(gca,'Position',[left bottom width height])
    pbaspect([1 rate 1])
    axis off
end

s = size(images{1});
s2 = s(2);

% Se incorporan los mensajes de texto adicionales asociados a cada imagen
for jj = 1:length(extra)    
    message = extra{jj};
    l = strlength(message);
    t = text(s2/2, -25, message, 'Parent', h(jj));
    set(t,'FontSize',14);
    set(t,'HorizontalAlignment','center');
end
