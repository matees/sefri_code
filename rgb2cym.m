function [] = rgb2cym(name)

%%
% Esta funcion se utiliza para visualizar la variacion cromatica entre los
% modelos de color RGB y CYMK. Utilizando un perfil ICC basado en el modelo
% CYMK se genera una representacion de la imagen original con este
% modelo de color (CYMK), y la imagen resultante se guarda en un fichero
% auxiliar para compararla con la imagen original en RGB.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

rgbComponents = imread(name);

% Perfiles ICC de entrada (RGB) y salida (CYMK)
inprof = iccread('sRGB.icm');
outprof = iccread('PSOuncoated_v3_FOGRA52.icc');

% Se modifica la representacion de la imagen usando los perfiles ICC de
% entrada y salida
c = makecform('icc', inprof,outprof);
imageCMY = applycform(rgbComponents,c);

path = './CYMK';
[success, message] = mkdir(path);
if ~success
    disp(message)
end

% Se guarda la imagen resultante en un fichero con extension .tif
ind1 = [strfind(name,'/') strfind(name,'\')];
ind2 = strfind(name,'.');
nameIm = name(ind1(end)+1:ind2-1);
name_ext = sprintf('%s.tif',nameIm);
name_ext2 = sprintf('%s_cmyk.tif',nameIm);

fullName = fullfile(path,name_ext);
fullName2 = fullfile(path,name_ext2);
imwrite(rgbComponents,fullName);
imwrite(imageCMY,fullName2);

% c = makecform('cmyk2srgb','RenderingIntent','AbsoluteColorimetric');
% imageRGB = applycform(imageCMY,c);

%diff = imabsdiff(rgbComponents,imageRGB);
% 
% images = {rgbComponents,imageRGB};
% plotTemplate(images,'Comparación RGB vs CYM');
