function [] = changeColormap(name)

%%
% Esta funcion permite representar la imagen original utilizando 3 paletas
% con 256, 64 y 8 colores. Las imagenes resultantes son mostradas por
% pantalla junto con la imagen original.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

in = imread(name);

% Se utilizan 3 paletas de color para representar la imagen original
% con un numero diferente de colores. No se aplica dithering sobre las 
% imagenes resultantes.
[indImage,map] = rgb2ind(in,256,'nodither');
out_256 = ind2rgb(indImage,map);

[indImage,map] = rgb2ind(in,64,'nodither');
out_64 = ind2rgb(indImage,map);

[indImage,map] = rgb2ind(in,8,'nodither');
out_8 = ind2rgb(indImage,map);

% Se muestra por pontalla el efecto de utilizar diferente numero de colores
% para representar la imagen original. 
images = {in,out_256,out_64,out_8};
message = {'Original', '256 colores', '64 colores','8 colores'};
plotTemplate(images,'Reduciendo el numero de colores',1:4,message);