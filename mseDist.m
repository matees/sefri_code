function [] = mseDist(name)

%%
% Esta funcion genera varias images de salida con diferentes tipo de 
% distorsion, pero que tienen el mismo MSE (Mean Square Error) respecto de 
% la señal original. Estas imagenes son mostradas por pantalla junto con su
% valor de PSNR, de forma que se puedan apreciar como diferentes tipos de 
% distorsion con diferente impacto visual, pueden dar lugar a un mismo
% valor de PSNR.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%


image = imread(name);
s = size(image);
s1 = s(1);
s2 = s(2);

% Cada tipo de distorsion se ajusta para que de lugar a una imagen con la
% misma PSNR de referencia (entorno a 25 dB)
psnr1 = 100;
var = 1;

while(psnr1>25)
    imNoisy1 = imgaussfilt(image,var);
    psnr1 = psnr(image,imNoisy1);
    var = var + 0.25;
end

% Distorsion tipo salt and pepper
psnr2=100;
var2 = 0.001;
while(psnr2>25)
    imNoisy2 = imnoise(image,'salt & pepper',var2);
    var2 = var2 + 0.001;
    psnr2 = psnr(image,imNoisy2);
end

% Ruido Gaussiano
psnr3 = 100;
var3 = 0.001;
while(psnr3>25)
    imNoisy3 = imnoise(image,'gaussian',0,var3);
    var3 = var3 + 0.001;
    psnr3 = psnr(image,imNoisy3);
end

% Efecto borde negro 
psnr4 = 100;
ind = 1;
while(psnr4>25 && ind<15)
    imNoisy4 = image;
    imNoisy4(1:ind,:,:) = 0;
    imNoisy4(s1-ind:s1,:,:) = 0;
    imNoisy4(:,1:ind,:) = 0;
    imNoisy4(:,s2-ind:s2,:) = 0;
    psnr4 = psnr(image,imNoisy4);
    ind = ind + 1;
end
if(ind>9)
    ind = 1;
    while(psnr4>25 && ind<15)
        imNoisy4 = image;
        imNoisy4(1:ind,:,:) = 0;
        imNoisy4(s1-ind:s1,:,:) = 0;
        imNoisy4(:,1:ind,:) = 0;
        imNoisy4(:,s2-ind:s2,:) = 0;
        psnr4 = psnr(image,imNoisy4);
        ind = ind +1;
    end
end

% Efecto espejo respecto al eje vertical
imNoisy5 = fliplr(image);
psnr5 = psnr(image,imNoisy5);

% Ajuste de contraste
psnr6 = 100;
var3 = 0.05;
while(psnr6>25)
    imNoisy6 = imadjust(image,[var3 var3 var3; 1-var3 1-var3 1-var3],[]);
    var3 = var3 + 0.01;
    psnr6 = psnr(image,imNoisy6);
end

% Uso de paleta con un numero colores pequeño
psnr7 = 100;
var7 = 16;
while(psnr7>25)
    [indImage,map] = rgb2ind(image,var7,'nodither');
    if(isa(image,'uint8'))
        imNoisy7 = im2uint8(ind2rgb(indImage,map));
    elseif(isa(image,'uint16'))
        imNoisy7 = im2uint16(ind2rgb(indImage,map));
    end
    var7 = var7 -1;
    psnr7 = psnr(image,imNoisy7);
end

% Compresion JPEG
var8 = 15;
psnr8 = 100;
while(psnr8>25)
    imwrite(image,'prueba.jpg','Quality',var8);
    imNoisy8 = imread('prueba.jpg');
    psnr8 = psnr(image,imNoisy8);
    var8 = var8 - 1;
end

% Se visualizan las imagenes resultantes junto con el valor exacto de PSNR
% en dos ventanas diferentes
images = {image,imNoisy7,imNoisy2,imNoisy6};
message = cell(1,4);
message{1} = sprintf('');
message{2} = sprintf('PSNR = %0.2f dB',psnr7);
message{3} = sprintf('PSNR = %0.2f dB',psnr2);
message{4} = sprintf('PSNR = %0.2f dB',psnr6);
plotTemplate(images,'Diferente impacto visual con igual MSE: Original, Bloque de colores, Salt & Pepper, Ajuste de Contraste',1:4,message);

images = {imNoisy1,imNoisy3,imNoisy8,imNoisy4};
message = cell(1,4);
message{1} = sprintf('PSNR = %0.2f dB',psnr1);
message{2} = sprintf('PSNR = %0.2f dB',psnr3);
message{3} = sprintf('PSNR = %0.2f dB',psnr8);
message{4} = sprintf('PSNR = %0.2f dB',psnr4);
plotTemplate(images,'Diferente impacto visual con igual MSE: Blurry, Ruido Gaussiano, Compresion, Banda de Negros',1:4,message);


