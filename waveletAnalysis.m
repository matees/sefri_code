function [] = waveletAnalysis(name)

%%
% Esta funcion realiza una decomposicion de tres niveles de la imagen
% original en frecuencia utilizando la Transformada Wavelet Discreta. Una
% vez obtenidos los coeficientes para cada nivel, se observa el impacto de 
% eliminar cada uno de los tres niveles por separado en la calidad de la
% imagen codificada.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

imageRGB = imread(name);

y = im2double(rgb2gray(imageRGB));

type = 'db4';
[C,S] = wavedec2(y,3,type);

%Reconstruction usign all the coefficients
imageOrig = waverec2(C,S,type);


%% Indices for the third-level coefficients
indA3 = 1:S(1,1)*S(1,2);
indH3 = indA3(end)+1:indA3(end) + S(2,1)*S(2,2);
indV3 = indH3(end)+1:indH3(end) + S(2,1)*S(2,2);
indD3 = indV3(end)+1:indV3(end) + S(2,1)*S(2,2);

%Indices for the second-level coefficients
indH2 = indD3(end)+1:indD3(end) + S(3,1)*S(3,2);
indV2 = indH2(end)+1:indH2(end) + S(3,1)*S(3,2);
indD2 = indV2(end)+1:indV2(end) + S(3,1)*S(3,2);

%Indices for the first-level coefficients
indH1 = indD2(end)+1:indD2(end) + S(4,1)*S(4,2);
indV1 = indH1(end)+1:indH1(end) + S(4,1)*S(4,2);
indD1 = indV1(end)+1:indV1(end) + S(4,1)*S(4,2);

%% Removing the first-level coefficients
newC = C;
newC([ indV1 indD1])=0;
%Reconstruction usign the remaining coefficients
imageRec1 = waverec2(newC,S,type);
imageRec1(imageRec1<0) = 0;
imageRec1(imageRec1>255) = 255;


%% Removing the second-level coefficients
newC = C;
newC([ indV2 indD2])=0;
%Reconstruction usign the remaining coefficients
imageRec2 = waverec2(newC,S,type);
imageRec2(imageRec2<0) = 0;
imageRec2(imageRec2>255) = 255;

%% Removing the third-level coefficients
newC = C;
newC([ indV3 indD3])=0;
%Reconstruction usign the remaining coefficients
imageRec3 = waverec2(newC,S,type);
imageRec3(imageRec3<0) = 0;
imageRec3(imageRec3>255) = 255;

images = {imageOrig,imageRec1,imageRec2,imageRec3};
plotTemplate(images,'Impacto visual de variaciones en diferentes frecuencias');