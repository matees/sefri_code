function [] = fisheye(name)

%%
% Esta funcion aplica el efecto ojo de pez sobre la imagen que se le pasa
% como parametro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

% Se carga la imagen original y se ajusta su tamaño para poder aplicar
% el efecto de ojo de pez
rgbImage = imread(name);
[r, c, ~] = size(rgbImage);      
nPad = ceil((c-r)/2);
rgbImage = cat(1, ones(nPad, c, 3), rgbImage, ones(nPad, c, 3));

% Se define la estructura para poder aplicar el efecto deseado sobre la
% imagen original
options = [c c 1.5];  
tf = maketform('custom', 2, 2, [], @fisheye_inverse, options);
           
% Se aplica la transformacion a la imagen   
newImage = imtransform(rgbImage, tf);
 
% Se muestra la imagen original y la imagen obtenida tras aplicarle el
% efecto deseado
images = {rgbImage,newImage};
plotTemplate(images,'Efecto de lente tipo ojo de pez');

end

% Funcion que define la transformacion necesaria para conseguir el efecto
% de ojo de pez
function U = fisheye_inverse(X, T)

  imageSize = T.tdata(1:2);
  exponent = T.tdata(3);
  origin = (imageSize+1)./2;
  scale = imageSize./2;

  x = (X(:, 1)-origin(1))/scale(1);
  y = (X(:, 2)-origin(2))/scale(2);
  R = sqrt(x.^2+y.^2);
  theta = atan2(y, x);

  cornerScale = min(abs(1./sin(theta)), abs(1./cos(theta)));
  cornerScale(R < 1) = 1;
  R = cornerScale.*R.^exponent;

  x = scale(1).*R.*cos(theta)+origin(1);
  y = scale(2).*R.*sin(theta)+origin(2);
  U = [x y];

end