function [] = adjustHue(name)

%%
% Esta funcion permite ajustar el tono de una imagen; ademas muestra el
% histograma de tonos correspondiente a la imagen que se pasa como parametro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen
%
% Parametros de salida:
%
%%

image = imread(name);
s = size(image);
s1 = s(1);
s2 = s(2);
rate = s1/s2;

hlim = 550/rate;

% Se obtienen las componentes HSL de la imagen
rgbDouble = im2double(image);
hsl=rgb2hsl(rgbDouble);

fig = uifigure('Name','Ajustar el tono de la imagen','Position',[90 100 hlim+420 550]);
axIma = uiaxes(fig,'Position',[10 0 hlim 550]);
imshow(image,'Parent',axIma);
set(axIma,'visible','off');

% Se calcula y se muestra el histograma de tonos de la imagen
axHist = uiaxes(fig,'Position',[hlim+6 270 390 220]);
h = hsl(:,:,1);
hRow = h(:);
bins = 0:0.02:1;
[f,b] = hist(hRow,bins);
pol = area(b,f,'Parent',axHist);
pol.FaceColor = [1 0.55 0.8];
axesFig= findobj(fig);
axesFig(2).Color =[ 0.95 0.95 0.95];
axHist.XLim = [0 1];
axHist.YTick = [];
axHist.Title.String = 'Histograma del tono';

c= hsv(64);
a = cat(3,repmat(c(:,1)',5,1), repmat(c(:,2)',5,1), repmat(c(:,3)',5,1));
axColor = uiaxes(fig,'Position',[hlim-15 170 440 120]);
imshow(a,'Parent',axColor);

label = uilabel(fig);
label.HorizontalAlignment = 'center';
label.FontSize = 14;
label.FontColor = [0 0 1];
label.Position = [hlim + 117 170 170 18];
label.Text = 'Ajuste de Tono';

param = cell(5);
param{1}= hsl;
param{2} = fig;
param{3} = axIma;
param{4} = axHist;
param{5} = axColor;

sld = uislider(fig,'Position',[hlim+75 140 250 3],  ...
    'ValueChangedFcn',@(sld,event) updateInt(sld,param));
sld.Limits = [0 1];
sld.Value=0;

end

%Manejador para actualizar la imagen cuando se ajusta el tono
function updateInt(sld,event)

    value = sld.Value;
        
    hsl = event{1};
    fig = event{2};
    
    h = hsl(:,:,1);
    s = hsl(:,:,2);
    l = hsl(:,:,3);
    
    % Se actualiza el tono de la imagen en función del valor seleccionado
    % en la regla de ajuste
    h = h + value;
    h = mod(h,1);
    hsl = cat(3,h,s,l);
    
    rgb=hsl2rgb(hsl);
    axesHandlesToChildObjects = findobj(fig, 'Type', 'image');
    if ~isempty(axesHandlesToChildObjects)
        delete(axesHandlesToChildObjects);
    end
    ax = event{3};
    imshow(rgb,'Parent',ax); 
    
    % Se actualiza ademas el historgrama de tonos
    axesFig = findobj(fig,'Type', 'line');
    delete(axesFig);
    %ax = uiaxes(fig,'Position',[hlim+6 270 390 220]);
    axh = event{4};
    hRow = h(:);
    bins = 0:0.02:1;
    [f,b] = hist(hRow,bins);
    pol = area(b,f,'Parent',axh);
    pol.FaceColor = [1 0.55 0.8];

    c= hsv(64);
    a = cat(3,repmat(c(:,1)',5,1), repmat(c(:,2)',5,1), repmat(c(:,3)',5,1));
    axColor = event{5};
    imshow(a,'Parent',axColor);
end