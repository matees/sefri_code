function [] = plotLumaChroma(name)

%% 
% Esta función obtiene y dibuja las componentes de luma(luminosidad) y de
% chroma(color) de la imagen que se pasa como parámetro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

%Se obtienen las componentes YCbCr de la imagen
yCbCrComponents = rgb2ycbcr(rgbComponents);
%Se separa individualmente cada una de las componentes 
y = yCbCrComponents(:,:,1);
Cb = yCbCrComponents(:,:,2);
Cr = yCbCrComponents(:,:,3);

%Se separa señal de luma de la parte cromática
res = size(rgbComponents);
a = zeros(res(1:2));
luma = cat(3,y,y,y);
chroma = cat(3,a,Cb,Cr);
chromaRGB = ycbcr2rgb(chroma);

%Se recupera la imagen original a partir de sus componentes YCC
imageRec = ycbcr2rgb(yCbCrComponents);

%Se muestra por pontalla cada componente YCbCr en RGB
%images = {rgbComponents,luma,chromaRGB,imageRec};
images = {luma,chromaRGB};
message = {'Luminancia', 'Parte Cromatica'};
plotTemplate(images, 'Luminancia y Parte Cromatica',1:2,message);
