function [] = adjustIntensity(name)

%%
% Esta funcion permite ajustar la intensidad del color en una imagen; ademas
% muestra el histograma de la intensidad correspondiente a la imagen que se 
% pasa como parametro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen
%
% Parametros de salida:
%
%%

image = imread(name);
s = size(image);
s1 = s(1);
s2 = s(2);
rate = s1/s2;

hlim = 550/rate;

% Se obtienen las componentes HSL de la imagen
rgbDouble = im2double(image);
hsl=rgb2hsl(rgbDouble);

fig = uifigure('Name','Ajustar la intensidade de la imagen','Position',[90 100 hlim+420 550]);
axIma = uiaxes(fig,'Position',[10 0 hlim 550]);
imshow(image,'Parent',axIma);
set(axIma,'visible','off');

% Se calcula y se muestra el histograma de intensidad de la imagen
axHist = uiaxes(fig,'Position',[hlim+6 270 390 220]);
l = hsl(:,:,3);
lRow = l(:);
bins = 0:0.02:1;
[f,b] = hist(lRow,bins);
pol = area(255*b,f,'Parent',axHist);
pol.FaceColor = [1 0.55 0.8];
axesFig= findobj(fig);
axesFig(2).Color =[ 0.95 0.95 0.95];
axHist.XLim = [0 255];
axHist.YTick = [];
axHist.Title.String = 'Histograma de la intensidad';

% Se calculan algunos parametros relacionados con la distribucion de
% la intensidad de los colores en la imagen
media = round(255*mean(lRow));
mediana = round(255*median(lRow));
claro = round(100*(sum(lRow>0.85)/length(lRow)));
oscuro = round(100*(sum(lRow<0.15)/length(lRow)));
data = [ media mediana oscuro claro]; 
t = uitable(fig,'Data',data,'Position',[hlim + 20 190 350 44]);
t.ColumnName = {'Media','Mediana','Zona Oscura', 'Zona Clara'};

label = uilabel(fig);
label.HorizontalAlignment = 'center';
label.FontSize = 14;
label.FontColor = [0 0 1];
label.Position = [hlim + 117 130 170 18];
label.Text = 'Ajuste de Intensidad';

param = cell(5);
param{1}= hsl;
param{2} = fig;
%param{3} = hlim;
param{3} = axIma;
param{4} = axHist;
param{5} = t;

sld = uislider(fig,'Position',[hlim+100 100 200 3],  ...
    'ValueChangedFcn',@(sld,event) updateInt(sld,param));
sld.Limits = [-150 150];
sld.Value=0;

end

%Manejador para actualizar la imagen cuando se ajusta la intensidad
function updateInt(sld,event)
    value = sld.Value;
    
    hsl = event{1};
    fig = event{2};
    
    h = hsl(:,:,1);
    s = hsl(:,:,2);
    l = hsl(:,:,3);
    
    % Se actualiza la intensidad de los colores en la imagen en función del
    % valor seleccionado en la regla de ajuste
    l = l + value/255;
    l(l>1)=1;
    l(l<0)=0;
    hsl = cat(3,h,s,l);
    
    rgb=hsl2rgb(hsl);
    axesHandlesToChildObjects = findobj(fig, 'Type', 'image');
    if ~isempty(axesHandlesToChildObjects)
        delete(axesHandlesToChildObjects);
    end
    %hlim = event{3};
    %ax = uiaxes(fig,'Position',[10 0 hlim 550]);
    ax = event{3};
    imshow(rgb,'Parent',ax); 
    
    % Se actualiza ademas el historgrama de la intensidad
    axesFig = findobj(fig,'Type', 'line');
    delete(axesFig);
    %ax = uiaxes(fig,'Position',[hlim+6 270 390 220]);
    axh = event{4};
    lRow = l(:);
    bins = 0:0.02:1;
    [f,b] = hist(lRow,bins);
    pol = area(255*b,f,'Parent',axh);
    pol.FaceColor = [1 0.55 0.8];
    
    % Se actualizan los parametros para caracterizar la distribucion de
    % la intensidad de los colores en la imagen
    media = round(255*mean(lRow));
    mediana = round(255*median(lRow));
    claro = round(100*(sum(lRow>0.85)/length(lRow)));
    oscuro = round(100*(sum(lRow<0.15)/length(lRow)));
    data = [ media mediana oscuro claro];
    t = event{5};
    t.Data = data;
end