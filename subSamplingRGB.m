function [] = subSamplingRGB(name,type)

%% 
% Esta función aplica el submuestreo especificado por type sobre las
% componentes RGB de la imagen en el fichero especificado por el
% parámetro name
%
% Parámetros de entrada:
%   name: nombre del fichero de la imagen con extensión 
%   type: array con tres elementos que indica el tipo de submuestreo 
%         (p.ej: [4 2 2])
%
% Parámetros de salida:
%   in: componentes RGB de la imagen original
%   out: componentes RGB de la imagen submuestreada

%%


j = type(1);
a = type(2);
b = type(3);

% Ejemplos de configuraciones no soportadas
% 4:3:3 - 4:2:3 - 4:0:2
if((a==0 || mod(j,a) || mod(j,b)) && (b~=0))
    error('Subsampling type not supported');
end

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);
in = rgbComponents;

%Separacion entre los pixels con los que sí nos quedamos
gap1 = j/a;
gap2 = j/b;

%Para los casos 4:4:4 o 8:8:8 no se hace realmente nada
if (gap1==1 && gap2==1)
    out = rgbComponents;
    return;
end

%Separamos las componentes RGB
rComponent = rgbComponents(:,:,1);
gComponent = rgbComponents(:,:,2);
bComponent = rgbComponents(:,:,3);

%% Preprocesado
%Comprobamos que el número de columnas es múltiplo del factor de
%submuestreo; en caso contrario necesitamos añadir columnas
res = size(rComponent);
paid = 0;
if(mod(res(2),j))
   paid = j - mod(res(1),j);
end
%Se añaden las columnas necesarias
rComponent = [rComponent repmat(rComponent(:,end),1,paid)];
gComponent = [gComponent repmat(gComponent(:,end),1,paid)];
bComponent = [bComponent repmat(bComponent(:,end),1,paid)];
%Se comprueba que el numero de filas es par; en caso contrario se añade una
if(mod(res(1),2))
   rComponent = [rComponent; rComponent(end,:)];
   gComponent = [gComponent; gComponent(end,:)];
   bComponent = [bComponent; bComponent(end,:)];
end
%%

%Se aplica el submuestreo correspondiente sobre las componentes RGB
if(gap1~=1)
  for ii = 2:gap1
      rComponent(1:2:end,ii:gap1:end) = rComponent(1:2:end,1:gap1:end);
      gComponent(1:2:end,ii:gap1:end) = gComponent(1:2:end,1:gap1:end);
      bComponent(1:2:end,ii:gap1:end) = bComponent(1:2:end,1:gap1:end);
  end
end

if(gap2~=1)
    if (b~=0)
        for ii = 2:gap2
            rComponent(2:2:end,ii:gap2:end) = rComponent(2:2:end,1:gap2:end);
            gComponent(2:2:end,ii:gap2:end) = gComponent(2:2:end,1:gap2:end);
            bComponent(2:2:end,ii:gap2:end) = bComponent(2:2:end,1:gap2:end);
        end
    else
        %En el caso de b=0, las columnas pares directamente se representan
        %por las impares
        rComponent(2:2:end,:) = rComponent(1:2:end,:);
        gComponent(2:2:end,:) = gComponent(1:2:end,:);
        bComponent(2:2:end,:) = bComponent(1:2:end,:);
    end
end

%Se eliminan las columnas si se añadieron anteriormente
rComponent(:,end-paid+1:end) = [];
gComponent(:,end-paid+1:end) = [];
bComponent(:,end-paid+1:end) = [];
%Se eliminan las filas si se añadieron
if(mod(res(1),2))
   rComponent(end,:) = [];
   gComponent(end,:) = [];
   bComponent(end,:) = [];
end

out = cat(3, rComponent,gComponent,bComponent);

% Se muestran por pantalla la imagen original y la imagen resultante del
% submuestreo
images = {in,out};
titulo = sprintf('Submuestreo en las componentes RGB  %d : %d : %d',j,a,b);
plotTemplate(images, titulo);



