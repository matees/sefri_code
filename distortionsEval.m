function [] = distortionsEval(name,type)

%%
% Esta funcion se utiliza para evaluar la calidad de las imagenes
% obtenidas tras aplicar diferentes tipos de distorsion sobre la imagen
% original. El resultado de aplicar la funcion es una pantalla donde se
% comparan la imagen original y la imagen distorsionadas, y donde ademas se
% muestra el valor de SSIM y PSNR de la imagen resultante.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%   type: tipo de efecto / distorsion
%
% Parametros de salida:
%
%%

image = imread(name);
s = size(image);
s1 = s(1);
s2 = s(2);

% Se aplican diferentes tipos de distorsion a la imagen original en funcion
% del parametro de entrada type.
switch type
    
    % Desenfoque
    case 'blurry'
        imNoisy = imgaussfilt(image,3);
    
    % Granulado tipo salt and pepper    
    case 'saltandpepper'
        imNoisy = imnoise(image,'salt & pepper',0.01);
    
    % Ruido Gaussiano    
    case 'gaussian'
        imNoisy = imnoise(image,'gaussian',0,0.0035);
    
    % Borde exterior de dos lineas de pixels a negro    
    case 'block'
        imNoisy = image;
        imNoisy(1:2,:,:) = 0;
        imNoisy(s1-1:s1,:,:) = 0;
        imNoisy(:,1:2,:) = 0;
        imNoisy(:,s2-1:s2,:) = 0;
    
    % Flipped de los pixels de la imagen respecto del eje vertical    
    case 'flip'
        imNoisy = fliplr(image);
    
    % Cambio en el contraste de la imagen    
    case 'contrast'
        imNoisy = imadjust(image,[0.1 0.1 0.1; 0.9 0.9 0.9],[]);
    
    % Compression JPEG con nivel de calidad 5    
    case 'other'
%         [indImage,map] = rgb2ind(image,8,'nodither');
%         if(isa(image,'uint8'))
%             imNoisy = im2uint8(ind2rgb(indImage,map));
%         elseif(isa(image,'uint16'))
%             imNoisy = im2uint16(ind2rgb(indImage,map));
%         end
        imwrite(image,'prueba.jpg','Quality',5);
        imNoisy = imread('prueba.jpg');
end

% Se determina la SSIM de la imagen distorsionada respecto de la imagen
% original
if(strcmp(type,'gaussian'))
    ssimExp = ssim(image,imNoisy);
else
   ssimExp = getMSSIM(image,imNoisy);
end
% Se determina la PSNR de la imagen distorsionada respecto de la imagen
% original
psnrExp = psnr(image,imNoisy);

% Se muestran por pantalla las imagenes original y distorsionada, junto con 
% los valores de SSIM y PSNR obtenidos.
message = cell(1,2);
message{1} = sprintf('');
message{2} = sprintf(' PSNR = %0.2f dB      MSSIM = %0.2f',psnrExp,ssimExp);
images = {image,imNoisy};
plotTemplate(images,'MSSIM vs PSNR',[1 2], message);

