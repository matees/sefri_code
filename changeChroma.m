function [] = changeChroma(name)

%% 
% Esta función intercambia las componentes de color correspondientes al 
% azul y rojo en el modelo YCC y muestra por pantalla los cambios respecto
% a la imagen original.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

%Se obtienen las componentes YCbCr de la imagen
yCbCrComponents = rgb2ycbcr(rgbComponents);
%Se separa individualmente cada una de las componentes 
y = yCbCrComponents(:,:,1);
Cb = yCbCrComponents(:,:,2);
Cr = yCbCrComponents(:,:,3);

%Se intercambian las componentes de chroma y se recupera la
%imagen en RGB
YCrCb = cat(3,y,Cr,Cb);
imageInv = ycbcr2rgb(YCrCb);

%Se muestra por pontalla el efecto del intercambio
images = {rgbComponents,imageInv};
plotTemplate(images,'Invertir las componentes de crominancia');
