function [] = plotRGB(name)

%% 
% Esta función obtiene y dibuja las componentes RGB de la imagen que
% se pasa como primer párametro
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

% Se separan individualmente las componentes para ser mostradas
res = size(rgbComponents);
a = zeros(res(1:2));
rComponent = cat(3,rgbComponents(:,:,1),a,a);
gComponent = cat(3,a, rgbComponents(:,:,2),a);
bComponent = cat(3,a, a, rgbComponents(:,:,3));

%Se muestra por pontalla cada componente RGB
images = {rgbComponents,rComponent,gComponent,bComponent};
plotTemplate(images,'Componentes RGB');