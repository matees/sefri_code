function [] = icc_conversion(name,profile)

%%
% Esta funcion permite modificar la representación de una imagen usando el 
% perfil ICC que se le pasa como parametro. Ademas, se compara la imagen
% original con la imagen modificada para apreciar el efecto que tiene en la
% representacion de los colores la variacion del perfil ICC 
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%   profile: fichero que contiene el perfil ICC que se desea usar (con
%   extension)
%
% Parametros de salida:
%
%%

% Se carga la imagen original
imageIn = imread(name);

% Se leen los perfiles ICC de entrada (se asume Standard RGB) y salida
inprof = iccread('sRGB.icm');
outprof = iccread(profile);

% Se modifica la imagen para ajustarla al perfil ICC de salida
c = makecform('icc', inprof,outprof);
imageOut = applycform(imageIn,c);

%diff = imabsdiff(imageIn,imageOut);

% Se muestra por pantalla la imagen original y la imagen modificada
images = {imageIn,imageOut};
mesagge = cell(1,2);
message{1} = 'sRGB';
ind1 = [strfind(profile,'/') strfind(profile,'\')];
ind2 = strfind(profile,'.');
name = profile(ind1(end)+1:ind2-1);
message{2} = name;
plotTemplate(images,'Comparacion usando diferentes perfiles ICC',1:2,message);
