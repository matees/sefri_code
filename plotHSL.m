function [] = plotHSL(name)

%% 
% Esta función obtiene y dibuja las componentes HSL (Hue, Saturation, Lightness) 
% de la imagen que se le pasa como párametro.
%
% Parametros de entrada:
%   name: nombre del fichero que contiene la imagen (con extension)
%
% Parametros de salida:
%
%%

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);

rgbDouble = im2double(rgbComponents);

%Se obtienen las componentes HSL de la imagen
hsl = rgb2hsl(rgbDouble);

fullfig('Name', 'Componentes HSL');
c= hsv(64);
a = cat(3,repmat(c(:,1)',5,1), repmat(c(:,2)',5,1), repmat(c(:,3)',5,1));

% Se representan los historgramas correspondientes al tono, la saturacion y
% la intensidad de la imagen en una misma figura.
subplot(2,2,1)
h = hsl(:,:,1);
[v,bins] = hist(h(:),200);
set(gca,'Position',[0.025 0.6 0.4 0.3]);
plot(bins,v);
yticks([]);
title('Histograma de los tonos');

subplot(2,2,2)
s = hsl(:,:,2);
[v,bins] = hist(s(:),200);
set(gca,'Position',[0.525 0.6 0.4 0.3]);
plot(100*bins,v);
yticks([]);
xlim([0 100]);
title('Histograma de la saturacion');

subplot(2,2,4)
l = hsl(:,:,3);
[v,bins] = hist(l(:),200);
set(gca,'Position',[0.525 0.1 0.4 0.3]);
plot(255*bins,v);
yticks([]);
xlim([0 255]);
title('Histograma de la intensidad');

subplot(2,2,3)
set(gca,'Position',[0.025 0.4 0.4 0.2]);
imshow(a)

