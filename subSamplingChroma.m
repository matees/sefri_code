function [in, out] = subSamplingChroma(name,type)

%% 
% Esta función aplica el submuestreo especificado por type sobre las
% componentes de crominancia de la imagen en el fichero especificado por el
% parámetro name
%
% Parámetros de entrada:
%   name: nombre del fichero de la imagen con extensión 
%   type: array con tres elementos que indica el tipo de submuestreo 
%         (p.ej: [4 2 2])
%
% Parámetros de salida:
%   in: componentes RGB de la imagen original
%   out: componentes RGB de la imagen submuestreada

%%


j = type(1);
a = type(2);
b = type(3);

% Ejemplos de configuraciones no soportadas
% 4:3:3 - 4:2:3 - 4:0:2
if((a==0 || mod(j,a) || mod(j,b)) && (b~=0))
    error('Subsampling type not supported');
end

%Se obtienen las componentes RGB de la imagen
rgbComponents = imread(name);
in = rgbComponents;

%Separación entre los pixels de las crominancias con las que sí nos quedamos
gap1 = j/a;
gap2 = j/b;

%Para los casos 4:4:4 o 8:8:8 no se hace realmente nada
if (gap1==1 && gap2==1)
    out = rgbComponents;
    return;
end

%Se obtienen las componentes YCbCr de la imagen
yCbCr = rgb2ycbcr(rgbComponents);
y = yCbCr(:,:,1);
Cb = yCbCr(:,:,2);
Cr = yCbCr(:,:,3);

%% Preprocesado
%Comprobamos que el número de columnas es múltiplo del factor de
%submuestreo; en caso contrario necesitamos añadir columnas
res = size(rgbComponents(:,:,1));
paid = 0;
if(mod(res(2),j))
   paid = j - mod(res(2),j);
end
%Se añaden las columnas necesarias
Cb = [Cb repmat(Cb(:,end),1,paid)];
Cr = [Cr repmat(Cr(:,end),1,paid)];

%Se comprueba que el numero de filas es par; en caso contrario se añade una
if(mod(res(1),2))
   Cb = [Cb; Cb(end,:)];
   Cr = [Cr; Cr(end,:)];
end
%%

%Se aplica el submuestreo correspondiente sobre las componentes de
%crominancia
if(gap1~=1)
  for ii = 2:gap1
      Cb(1:2:end,ii:gap1:end) = Cb(1:2:end,1:gap1:end);
      Cr(1:2:end,ii:gap1:end) = Cr(1:2:end,1:gap1:end);
  end
end

if(gap2~=1)
    if (b~=0)
        for ii = 2:gap2
            Cb(2:2:end,ii:gap2:end) = Cb(2:2:end,1:gap2:end);
            Cr(2:2:end,ii:gap2:end) = Cr(2:2:end,1:gap2:end);
        end
    else
        %En el caso de b=0, las columnas pares directamente se representan
        %por las impares
        Cb(2:2:end,:) = Cb(1:2:end,:);
        Cr(2:2:end,:) = Cr(1:2:end,:);
    end
end

%Se eliminan las columnas si se añadieron 
Cb(:,end-paid+1:end) = [];
Cr(:,end-paid+1:end) = [];
%Se eliminan las filas si se añadieron
if(mod(res(1),2))
   Cb(end,:) = [];
   Cr(end,:) = [];
end

%Se obtienen las componentes RGB correspondientes
newyCbCr = cat(3,y,Cb,Cr);
out = ycbcr2rgb(newyCbCr);

% Se muestran por pantalla la imagen original y la imagen resultante del
% submuestreo
images = {in,out};
titulo = sprintf('Submuestreo de crominancia  %d : %d : %d',j,a,b);
plotTemplate(images,titulo);




